# About

This directory contains the environment and dependencies for Task 1 for the Data Engineer role at Adaptavist.

It uses a command line interface (CLI) utility that replicates an Amazon Managed Workflows for Apache Airflow (MWAA) environment locally.

The CLI builds a Docker container image locally that’s similar to a MWAA production image. This allows you to run a local Apache Airflow environment to develop and test DAGs, custom plugins, and dependencies before deploying to MWAA.

Once you follow the steps below to mount the application, the Apache Airlfow UI will be available at: <http://localhost:8080/>.

# Task Description

For this task, you are asked to:
- Using the alpha-vantage Python library (https://pypi.org/project/alpha-vantage) that is already included in this local Airflow environment provided, create an Airflow dag that will ingest the currency conversion rate from GBP to USD on any given day.
- Insert the ingest data to S3. The developer environment in the repo is already set up with a mock S3 stack and connection, so you don’t need to worry about credentials etc as long as you use the defaults. We suggest using the built-in S3 Hook.
- You are free to set it up however you see fit. To get you started, we have created a dag definition file called “ingest_fx_rate.py” and a custom operator skeleton called “fx_rate_operator.py”, with some comments as to what we expect to see.

# Developer Environment and Getting Started

## Prerequisites

- **macOS**: [Install Docker Desktop](https://docs.docker.com/desktop/).
- **Linux/Ubuntu**: [Install Docker Compose](https://docs.docker.com/compose/install/) and [Install Docker Engine](https://docs.docker.com/engine/install/).
- **Windows**: Windows Subsystem for Linux (WSL) to run the bash based command `mwaa-local-env`. Please follow [Windows Subsystem for Linux Installation (WSL)](https://docs.docker.com/docker-for-windows/wsl/) and [Using Docker in WSL 2](https://code.visualstudio.com/blogs/2020/03/02/docker-in-wsl2), to get started.

## Get started

### Step one: Building the Docker image

Build the Docker container image using the following command:

```bash
./mwaa-local-env build-image
```

**Note**: it takes several minutes to build the Docker image locally.

### Step two: Running Apache Airflow

Run a local Apache Airflow environment that is a close representation of MWAA by configuration.

```bash
./mwaa-local-env start
```

To stop the local environment, Ctrl+C on the terminal and wait till the local runner and the postgres containers are stopped.

### Step three: Accessing the Airflow UI

By default, the `bootstrap.sh` script creates a username and password for your local Airflow environment.

- Username: `admin`
- Password: `test`