from airflow import DAG
from airflow.models.variable import Variable
from custom_operator.fx_rate_operator import ForeignExchangeIngester
from datetime import datetime


env: str = Variable.get("ENVIRONMENT", default_var="dev")
dag_id = "test_dag"
# The library we use to get currency exchange rates is connected to a free to use API.
# We stored our key in an Airflow variable.
# In order to test the full ingest, you can request a free key here : https://www.alphavantage.co/support/#api-key
API_KEY: str =  Variable.get("FX_API_KEY")

default_args = {
                "start_date": datetime(2021, 1, 1)
                }

# We would like the dag to run every day at 5.28am, and we would like to backfill all the dates from the start time
# to when it gets turned on.                