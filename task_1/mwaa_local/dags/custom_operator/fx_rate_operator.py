from pprint import pprint
from airflow.utils.decorators import apply_defaults
from airflow.models.baseoperator import BaseOperator
from alpha_vantage.foreignexchange import ForeignExchange
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
import json


class ForeignExchangeIngester(BaseOperator):
    # TASK 1: We expect this class to be configurable,
    # accepting a source and target currency, as well as an API key,
    # and a target bucket and file in S3.

    """
      

    """
    @apply_defaults
    def __init__(self,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        

    def execute(self, context):
        # This function should be able to retrieve the rate based
        # on the parameters passed to the class instance on initiliation
        # and write the totality of the output as a JSON file to S3, again
        # taking into account the parameters passed to it.
    