# README #

This repository contains two tasks that are part of the Technical Assessment stage of the Data Engineer position at Adaptavist.

### How do I get set up? ###

Each subdirectory contains a separate README with instructions on how to get setup and the requirements of the task.