

def main():
  # In this script, we expect you to implement the code to 
  # 1. retrieve the data from the API : https://weatherstack.com/
  # 2. add the new field to the data that will contain the current timestamp in the format of “YYYY-MM-DD HH:MM:SS”
  # 3. write the output as a CSV file to local storage, including the new field


if __name__ == "__main__":
  main()