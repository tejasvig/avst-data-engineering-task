# Task Description

Using the free plan of the publicly available API https://weatherstack.com/, create a short Python script that will
- ingest the current weather data for London from the API
- create a new field that will contain the current timestamp in the format of “YYYY-MM-DD HH:MM:SS”
- write it out as a CSV file to local storage, including the additional field as a column

The outputted CSV should have the following columns:
- obtained from the API: 'observation_time', 'temperature', 'weather_code', 'weather_icons’, 'weather_descriptions', 'wind_speed', 'wind_degree', 'wind_dir', 'pressure', 'precip', 'humidity', 'cloudcover', 'feelslike', 'uv_index', 'visibility', 'is_day'
- added column: ‘current_timestamp’

It should only have one row for the current date’s weather provided by the API.

We have provided the skeleton file app.py in this directory to get you started.

# Requirements and dependencies

If you need to use any additional Python packages to complete this task, please add them to the requirements.txt file included in this directory.